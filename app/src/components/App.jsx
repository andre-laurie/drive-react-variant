import React, { useCallback, useEffect, useState } from 'react';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import { api } from '../api/DriveAPI';
import { WithNavigationLayout } from './WithNavigationLayout';
import { Categories } from './Categories';
import { Category } from './Category';
import { Review } from './Review';
import { Login } from './Login';
import { Orders } from './Orders';
import { Order } from './Order';
import { RequireAuth } from './RequireAuth';

export const CartContext = React.createContext();
export const CustomerContext = React.createContext();

export function App() {
	const [cart, setCart] = useState({ amount: 0, articles: [] });
	const [customer, setCustomer] = useState();

	const reloadCart = useCallback(() => {
		console.log('reloadCart');
		api.getCurrentCart().then(setCart).catch(console.error);
	}, [setCart]);

	const reloadCustomer = useCallback(() => {
		console.log('reloadCustomer');
		api.getCurrentCustomer().then(setCustomer).catch(console.error);
	}, [setCustomer]);

	const addToCart = useCallback(
		(article) => {
			api
				.addToCart(article)
				.then((added) => {
					if (added) {
						reloadCart();
					}
				})
				.catch(console.error);
		},
		[reloadCart]
	);

	const removeFromCart = useCallback(
		(article) => {
			api
				.removeFromCart(article)
				.then((removed) => {
					if (removed) {
						reloadCart();
					}
				})
				.catch(console.error);
		},
		[reloadCart]
	);

	useEffect(() => {
		reloadCart();
		reloadCustomer();
	}, [reloadCart, reloadCustomer]);

	return (
		<CustomerContext.Provider value={{ customer, reloadCustomer }}>
			<CartContext.Provider
				value={{ cart, reloadCart, addToCart, removeFromCart }}
			>
				<BrowserRouter>
					<Routes>
						<Route element={<WithNavigationLayout />}>
							<Route index element={<Categories />} />
							<Route
								path="/catalog/categories/:categoryId"
								element={<Category />}
							/>
							<Route path="/cart/review" element={<Review />} />
							<Route
								path="/customer/orders"
								element={
									<RequireAuth>
										<Orders />
									</RequireAuth>
								}
							/>
							<Route
								path="/customer/order/:orderId"
								element={
									<RequireAuth>
										<Order />
									</RequireAuth>
								}
							/>
						</Route>
						<Route path="/customer/login" element={<Login />} />
					</Routes>
				</BrowserRouter>
			</CartContext.Provider>
		</CustomerContext.Provider>
	);
}
