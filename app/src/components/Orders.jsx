import { useEffect, useState } from 'react';
import { Link, useLocation } from 'react-router-dom';
import { FaListAlt } from 'react-icons/fa';
import { api } from '../api/DriveAPI';
import { Price } from './Price';
import { DateFormatter } from './DateFormatter';
import { Alert, Card, Table } from 'react-bootstrap';

export function Orders() {
	const [orders, setOrders] = useState([]);
	const location = useLocation();

	useEffect(() => {
		api.getAllOrders().then(setOrders).catch(console.error);
	}, [setOrders]);

	if (orders.length === 0) return 'Aucune commande';

	const confirmation = (location.state && location.state.message) || undefined;

	return (
		<>
			{confirmation && <Alert variant="success">{confirmation}</Alert>}
			<Card>
				<Card.Header>Liste des commandes</Card.Header>
				<Card.Body>
					<Table hover>
						<thead>
							<tr>
								<th>#</th>
								<th>Date de commande</th>
								<th>Date de livraison</th>
								<th>Montant</th>
								<th>Statut</th>
							</tr>
						</thead>
						<tbody>
							{orders.map((order) => (
								<Order key={order.id} order={order} />
							))}
						</tbody>
					</Table>
				</Card.Body>
			</Card>
		</>
	);
}

function Order({ order }) {
	return (
		<tr>
			<td>
				<Link to={`/customer/order/${order.id}`}>
					<FaListAlt /> {order.id}
				</Link>
			</td>
			<td>
				<DateFormatter date={order.createOn} />
			</td>
			<td>
				<DateFormatter date={order.deliveryDate} />
			</td>
			<td>
				<Price price={order.amount} />
			</td>
			<td>{order.currentStatus}</td>
		</tr>
	);
}
