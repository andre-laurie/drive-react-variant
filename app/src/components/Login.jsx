import { useState, useContext } from 'react';
import { Alert, Form, Button, FloatingLabel } from 'react-bootstrap';
import { useLocation, useNavigate } from 'react-router-dom';
import { api } from '../api/DriveAPI';
import { CartContext, CustomerContext } from './App';

export function Login() {
	const [errorMessage, setErrorMessage] = useState('');
	const [validated, setValidated] = useState(false);
	const { reloadCart } = useContext(CartContext);
	const { reloadCustomer } = useContext(CustomerContext);
	const location = useLocation();
	const from = (location.state && location.state.from) || '/';
	const navigate = useNavigate();
	console.log(location);

	function handleSubmit(event) {
		event.preventDefault();
		event.stopPropagation();

		const form = event.currentTarget;

		if (form.checkValidity() === true) {
			const data = new FormData(event.target);
			api
				.login(data)
				.then((response) => {
					if (response.status === 'OK') {
						reloadCart();
						reloadCustomer();
						navigate(from, {
							state: { login: response.message },
							replace: true,
						});
					} else {
						setErrorMessage(response.message);
					}
				})
				.catch(console.error);
		}

		setValidated(true);
	}

	return (
		<>
			<Alert variant="info">
				<Alert.Heading>Remarque :</Alert.Heading>
				Il y a 3 comptes prédéfinis dont les e-mail / mot de passe sont :
				<ul>
					<li>Customer A : a@a / a</li>
					<li>Customer B : b@b / b</li>
					<li>Customer C : c@c / c</li>
				</ul>
			</Alert>

			<Form
				noValidate
				validated={validated}
				onSubmit={handleSubmit}
				className="mx-auto"
				style={{ maxWidth: '330px' }}
			>
				{errorMessage && <Alert variant="danger">{errorMessage}</Alert>}
				<h2>Accéder à votre compte</h2>

				<FloatingLabel label="Email address" className="mb-3">
					<Form.Control
						placeholder="Adresse e-mail"
						required
						autoFocus
						type="email"
						name="email"
					/>
				</FloatingLabel>
				<FloatingLabel label="Password" className="mb-3">
					<Form.Control
						placeholder="Mot de passe"
						required
						type="password"
						name="password"
					/>
				</FloatingLabel>
				<Button variant="primary" size="lg" type="submit">
					Valider
				</Button>
			</Form>
		</>
	);
}
