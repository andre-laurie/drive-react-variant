import { useState, useEffect } from 'react';
import { NavLink } from 'react-router-dom';
import { api } from '../api/DriveAPI';

export function Categories() {
  const [categories, setCategories] = useState([]);

  useEffect(() => {
    api.getAllCategories().then(setCategories).catch(console.error);
  }, []);

  if (categories.length === 0) return 'Aucune catégorie';

  return (
    <ul className="categories">
      {categories.map((category) => (
        <li key={category.id}>
          <NavLink to={'/catalog/categories/' + category.id}>
            <img src={'/img/cat0' + category.id + '.jpg'} alt="" />
            <br />
            {category.name}
          </NavLink>
        </li>
      ))}
    </ul>
  );
}
