import { render, screen } from '@testing-library/react';
import WithNavigation from './WithNavigation';

test('renders learn react link', () => {
  render(<WithNavigation />);
  const linkElement = screen.getByText(/learn react/i);
  expect(linkElement).toBeInTheDocument();
});
