import { useContext, useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { Button, Card, Col, Row, Placeholder } from 'react-bootstrap';
import { FaChartBar, FaShoppingCart } from 'react-icons/fa';
import { api } from '../api/DriveAPI';
import { CartContext } from './App';
import { Price } from './Price';
import { AddCounterRemoveGroup } from './AddCounterRemoveGroup';

export function Category() {
	const [{ articles, loaded }, setArticles] = useState({
		articles: [],
		loaded: false,
	});
	const params = useParams();
	const categoryId = params.categoryId;

	useEffect(() => {
		api
			.getArticlesInCategory(categoryId)
			.then((articles) => setArticles({ articles, loaded: true }))
			.catch(console.error);
	}, [setArticles, categoryId]);

	const { cart } = useContext(CartContext);

	const articlesInCart = cart.articles;

	if (loaded && articles.length === 0) return 'Aucun article dans la catégorie';

	let cards;
	if (!loaded)
		cards = Array(10)
			.fill()
			.map((_, i) => <PlaceholderArticle key={i} />);
	else
		cards = articles.map((article) => {
			const nbInCart = articlesInCart
				.map((articleInCart) =>
					articleInCart.id === article.id ? articleInCart.qty : 0
				)
				.reduce((x, y) => x + y, 0);
			return <Article key={article.id} article={article} nbInCart={nbInCart} />;
		});

	return <Row className="g-0">{cards}</Row>;
}

function Article({ article, nbInCart }) {
	const { addToCart } = useContext(CartContext);

	return (
		<Col xs={12} sm={6} md={4} lg={3}>
			<Card className="d-flex flex-column m-2 p-2">
				<div className="d-flex flex-sm-column mb-2">
					<img
						className="d-block mx-sm-auto order-sm-2"
						src={'https://static1.chronodrive.com' + article.img}
						alt=""
					/>
					<div className="order-sm-1">
						<div style={{ height: '70px', fontSize: '0.9rem' }}>
							<span className="text-primary">{article.name}</span>
							<br />
							<span className="text-secondary">
								<FaChartBar /> &nbsp;En stock {article.stock}
							</span>
						</div>
					</div>
				</div>
				<div className="d-flex justify-content-between align-items-baseline">
					<span style={{ fontSize: '1.2rem' }}>
						<strong>
							<Price price={article.price} />
						</strong>
					</span>
					{nbInCart !== 0 ? (
						<AddCounterRemoveGroup nbInCart={nbInCart} article={article} />
					) : (
						<Button
							variant="primary"
							children={<FaShoppingCart />}
							onClick={() => addToCart(article)}
						/>
					)}
				</div>
			</Card>
		</Col>
	);
}

function PlaceholderArticle() {
	return (
		<Col xs={12} sm={6} md={4} lg={3}>
			<Placeholder
				as={Card}
				className="d-flex flex-column m-2 p-2"
				animation="glow"
			>
				<div className="d-flex flex-sm-column mb-2">
					<img
						className="d-block mx-sm-auto order-sm-2 bg-light "
						width={186}
						height={186}
						// src={'https://static1.chronodrive.com' + article.img}
						alt=""
					/>
					<div className="order-sm-1">
						<div
							className="text-primary"
							style={{ height: '70px', fontSize: '0.9rem' }}
						>
							<Placeholder xs={2} /> <Placeholder xs={3} />{' '}
							<Placeholder xs={4} />
							<br />
							<span className="text-secondary">
								<FaChartBar /> <Placeholder xs={1} /> <Placeholder xs={2} />
							</span>
						</div>
					</div>
				</div>
				<div className="d-flex justify-content-between align-items-baseline">
					<Placeholder xs={3} size="lg" />
					<Placeholder.Button variant="primary" xs={2} />
				</div>
			</Placeholder>
		</Col>
	);
}
