import { useContext } from 'react';
import { useLocation, Navigate } from 'react-router-dom';
import { CustomerContext } from './App';

export function RequireAuth({ children }) {
	const location = useLocation();
	console.log(location);
	const { customer } = useContext(CustomerContext);

	if (customer === null) {
		return <Navigate to="/customer/login" state={{ from: location }} replace />;
	}
	return children;
}
