function formatPrice(intPrice) {
	return `${(intPrice / 100).toFixed(2)}€`;
}

export function Price({ price }) {
	return <>{formatPrice(price)}</>;
}
