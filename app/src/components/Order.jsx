import { useEffect, useState } from 'react';
import { Card, Table } from 'react-bootstrap';
import { useParams } from 'react-router-dom';
import { api } from '../api/DriveAPI';
import { Price } from './Price';

export function Order() {
	const params = useParams();
	const orderId = params.orderId;
	const [articles, setArticles] = useState([]);

	useEffect(() => {
		api.getArticlesInOrder(orderId).then(setArticles).catch(console.error);
	}, [orderId]);

	return (
		<>
			<Card>
				<Card.Header>Détail de la commande réf {orderId}</Card.Header>
				<Card.Body>
					<Table hover>
						<thead>
							<tr>
								<th>Nom</th>
								<th>Quantité</th>
								<th>Prix</th>
							</tr>
						</thead>
						<tbody>
							{articles.map((article) => (
								<Article key={article.id} article={article} />
							))}
						</tbody>
					</Table>
				</Card.Body>
			</Card>
		</>
	);
}

function Article({ article }) {
	return (
		<tr>
			<td>{article.name}</td>
			<td>{article.qty}</td>
			<td>
				<Price price={article.price} />
			</td>
		</tr>
	);
}
