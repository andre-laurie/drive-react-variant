import { useContext } from 'react';
import { Modal, Button } from 'react-bootstrap';
import { useNavigate } from 'react-router-dom';

import { CartContext } from './App';
import { Article } from './Article';

export function CartPanel({ handleClose, show }) {
  const { cart } = useContext(CartContext);

  const articles = cart.articles;
  const navigate = useNavigate();

  function closeAndReview() {
    handleClose();
    navigate('/cart/review');
  }

  return (
    <Modal show={show} onHide={handleClose} dialogClassName="m-0 ms-auto">
      <Modal.Header closeButton>
        <Modal.Title>Aperçu du panier</Modal.Title>
      </Modal.Header>
      <Modal.Body className="d-flex flex-column" style={{ overflowY: 'auto' }}>
        <Contents articles={articles} />
      </Modal.Body>
      {articles.length !== 0 && (
        <Modal.Footer>
          <Button variant="primary" onClick={closeAndReview}>
            Voir mon panier
          </Button>
        </Modal.Footer>
      )}
    </Modal>
  );
}

function Contents({ articles }) {
  if (articles.length) {
    return articles.map((article) => (
      <Article key={article.name} article={article} />
    ));
  } else return 'Aucun article dans le panier';
}
