export function DateFormatter(props) {
	return <>{new Date(props.date).toLocaleDateString()}</>;
}
