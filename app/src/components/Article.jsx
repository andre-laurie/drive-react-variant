import { Card } from 'react-bootstrap';
import { AddCounterRemoveGroup } from './AddCounterRemoveGroup';
import { Price } from './Price';

export function Article({ article }) {
	return (
		<Card className="d-flex flex-column m-2 p-2">
			<div className="d-flex flex-row mb-2">
				<img
					className="d-block"
					src={'https://static1.chronodrive.com' + article.img}
					width="80"
					height="80"
					alt=""
				/>
				<div
					className="text-primary"
					style={{ height: '70px', fontSize: '0.9rem' }}
				>
					{article.name}
				</div>
			</div>
			<div className="d-flex justify-content-between align-items-baseline">
				<span style={{ fontSize: '1.2rem' }}>
					<strong>
						<Price price={article.price} />
					</strong>
				</span>
				<AddCounterRemoveGroup nbInCart={article.qty} article={article} />
			</div>
		</Card>
	);
}
