import React, { useCallback, useState } from 'react';
import { Alert } from 'react-bootstrap';
import { Outlet, useLocation } from 'react-router-dom';
import { CartPanel } from './CartPanel';
import { Navigation } from './Navigation';

export function WithNavigationLayout() {
  const [show, setShow] = useState(false);
  const handleClose = useCallback(() => setShow(false), [setShow]);
  const handleShow = useCallback(() => setShow(true), [setShow]);
  const location = useLocation();
  const login = location.state && location.state.login;

  return (
    <>
      <Navigation handleShow={handleShow} />
      <CartPanel show={show} handleClose={handleClose} />
      <div style={{ paddingTop: '80px' }}>
        {login !== null && (
          <Alert variant="success">
            <Alert.Heading> {`Bienvenue ${login}`}</Alert.Heading>
            Les articles du panier ont été regroupés dans le panier actif.
          </Alert>
        )}
        <Outlet />
      </div>
    </>
  );
}
