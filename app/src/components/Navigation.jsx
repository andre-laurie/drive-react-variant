import { useContext } from 'react';
import { useLocation, useNavigate } from 'react-router-dom';
import { Navbar, Nav, Badge, NavItem, NavDropdown } from 'react-bootstrap';
import { NavLink } from 'react-router-dom';
import {
  FaStore,
  FaShoppingCart,
  FaUser,
  FaSignInAlt,
  FaList,
  FaSignOutAlt,
  FaCheckCircle,
} from 'react-icons/fa';
import { api } from '../api/DriveAPI';
import { CartContext, CustomerContext } from './App';
import { Price } from './Price';

export function Navigation(props) {
  const { cart, reloadCart } = useContext(CartContext);
  const { customer, reloadCustomer } = useContext(CustomerContext);
  const location = useLocation();
  const navigate = useNavigate();

  const nbArticles = cart.articles
    .map((article) => article.qty)
    .reduce((x, y) => x + y, 0);
  const amount = nbArticles === 0 ? 'Panier' : <Price price={cart.amount} />;
  const authenticated = customer !== null;

  function logout() {
    api
      .logout()
      .then(() => {
        reloadCart();
        reloadCustomer();
        navigate('/');
      })
      .catch(console.error);
  }

  return (
    <Navbar
      bg="white"
      variant="light"
      expand="md"
      fixed="top"
      className="shadow-sm px-1"
    >
      <Navbar.Brand as={NavLink} to="/">
        <FaStore /> &nbsp;My Web Drive
      </Navbar.Brand>
      <Nav className="d-flex order-md-1 ms-auto pe-2 pe-md-0">
        <Nav.Link role="button" href="#" onClick={props.handleShow}>
          <TwoRowItem
            row1={
              <>
                <FaShoppingCart />
                &nbsp;
                <Badge pill bg="success">
                  {nbArticles}
                </Badge>
              </>
            }
            row2={amount}
          />
        </Nav.Link>
      </Nav>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse id="basic-navbar-nav">
        <Nav>
          <Nav.Link as={NavLink} to="/" children="Rayons" />
          {authenticated && (
            <Nav.Link as={NavLink} to="/customer/orders">
              Commandes
            </Nav.Link>
          )}
          <Nav.Link as={NavLink} to="#about">
            A propos
          </Nav.Link>
        </Nav>
        <NavDropdown
          as={NavItem}
          className="ms-auto"
          title={
            <TwoRowItem
              row1={
                <>
                  <FaUser />
                  {authenticated && (
                    <>
                      {' '}
                      <FaCheckCircle className="text-success" />
                    </>
                  )}
                </>
              }
              row2={'Compte'}
            />
          }
        >
          {!authenticated ? (
            <NavDropdown.Item
              as={NavLink}
              to="/customer/login"
              state={{ from: location }}
            >
              <FaSignInAlt />
              &nbsp;Connexion
            </NavDropdown.Item>
          ) : (
            <>
              <NavDropdown.Item as={NavLink} to="/customer/orders">
                <FaList />
                &nbsp;Commandes
              </NavDropdown.Item>
              <NavDropdown.Item onClick={logout}>
                <FaSignOutAlt />
                &nbsp;Déconnexion
              </NavDropdown.Item>
            </>
          )}
        </NavDropdown>
      </Navbar.Collapse>
    </Navbar>
  );
}

function TwoRowItem({ row1, row2 }) {
  return (
    <span className="d-inline-block text-center" style={{ lineHeight: '95%' }}>
      {row1}
      <br />
      {row2}
    </span>
  );
}
