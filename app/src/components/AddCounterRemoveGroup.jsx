import { useContext } from 'react';
import { Button, ButtonGroup } from 'react-bootstrap';
import { CartContext } from './App';

export function AddCounterRemoveGroup({ nbInCart, article }) {
	const { addToCart, removeFromCart } = useContext(CartContext);

	return (
		<ButtonGroup className="align-items-baseline align-items-stretch">
			<Button
				variant="outline-primary"
				children="-"
				onClick={() => removeFromCart(article)}
			/>
			<Button variant="outline-secondary" children={nbInCart} disabled />
			<Button
				variant="outline-primary"
				children="+"
				onClick={() => addToCart(article)}
			/>
		</ButtonGroup>
	);
}
