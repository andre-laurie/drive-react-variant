import { useContext, useState } from 'react';
import { Col, Row, Button, Alert } from 'react-bootstrap';
import { useLocation, useNavigate } from 'react-router-dom';
import { api } from '../api/DriveAPI';
import { Price } from './Price';
import { CartContext, CustomerContext } from './App';
import { Article } from './Article';

export function Review() {
	return (
		<>
			<Row>
				<Col md={12}>
					<h3>Votre panier</h3>
				</Col>
			</Row>
			<Contents />
		</>
	);
}

function Contents() {
	const { cart, reloadCart } = useContext(CartContext);
	const { customer } = useContext(CustomerContext);
	const authenticated = customer !== null;

	const articles = cart.articles;
	const location = useLocation();
	const navigate = useNavigate();

	const [missingArticles, setMissingArticles] = useState(null);

	if (articles.length === 0) return 'Aucun article dans le panier';

	let buttonText, callback;
	if (authenticated) {
		buttonText = 'Valider';
		callback = function () {
			api
				.validate()
				.then(({ validated, message }) => {
					reloadCart();
					if (validated) {
						navigate('/customer/orders', {
							state: { message },
						});
					} else {
						setMissingArticles(message);
					}
				})
				.catch(console.error);
		};
	} else {
		buttonText = 'Se connecter';
		callback = function () {
			navigate('/customer/login', {
				state: { from: location },
			});
		};
	}

	return (
		<>
			{missingArticles !== null && (
				<div className="row">
					<div className="col-md-12">
						<Alert variant="warning">
							<Alert.Heading>Avertissement:</Alert.Heading>
							{missingArticles}
						</Alert>
					</div>
				</div>
			)}
			<div className="d-flex flex-column">
				{articles.map((article) => (
					<Article key={article.id} article={article} />
				))}
			</div>
			<Footer
				amount={cart.amount}
				buttonText={buttonText}
				callback={callback}
			/>
		</>
	);
}

function Footer({ amount, buttonText, callback }) {
	return (
		<Row>
			<Col md={6}>
				<span className="h3">
					Total{' '}
					<span className="cart-title">
						<Price price={amount} />
					</span>
				</span>
				&nbsp;
			</Col>
			<Col md={6} className="text-right">
				<Button className="active" onClick={callback}>
					{buttonText}
				</Button>
			</Col>
		</Row>
	);
}
