async function doFetch(url, options) {
	const response = await fetch(url, options);
	if (!response.ok)
		throw Error(`API error: ${response.status} ${response.statusText}`);
	return response;
}

async function get(url, options = {}) {
	return await doFetch(url, options);
}

async function post(url, options = {}) {
	return await doFetch(url, {
		method: 'POST',
		...options,
	});
}

const api = {
	getCurrentCart: async function () {
		const response = await get('/cart/preview');
		return await response.json();
	},

	getCurrentCustomer: async function () {
		const response = await get('/customer/current');
		const simpleResponse = await response.json();
		return simpleResponse.message;
	},

	addToCart: async function (article) {
		const response = await post('/cart/add', {
			headers: { 'Content-Type': 'application/json;charset=UTF-8' },
			body: JSON.stringify(article.id),
		});
		const simpleResponse = await response.json();
		const status = simpleResponse.status;
		return status === 'OK';
	},

	removeFromCart: async function (article) {
		const response = await post('/cart/remove', {
			headers: { 'Content-Type': 'application/json;charset=UTF-8' },
			body: JSON.stringify(article.id),
		});
		const simpleResponse = await response.json();
		const status = simpleResponse.status;
		return status === 'OK';
	},

	getAllCategories: async function () {
		const response = await get('/catalog/categories');
		return await response.json();
	},

	getArticlesInCategory: async function (categoryId) {
		const url = `/catalog/category/${categoryId}`;
		const response = await get(url);
		return await response.json();
	},

	login: async function (formData) {
		const response = await post('/customer/login', {
			body: formData,
		});
		return await response.json();
	},

	logout: async function () {
		await post('/customer/logout');
	},

	getAllOrders: async function () {
		const response = await get('/customer/orders');
		return await response.json();
	},

	getArticlesInOrder: async function (orderId) {
		const response = await get(`/customer/order/${orderId}`);
		return await response.json();
	},

	validate: async function () {
		const response = await post('/cart/validate');
		const simpleResponse = await response.json();
		return {
			validated: simpleResponse.status === 'OK',
			message: simpleResponse.message,
		};
	},
};

export { api };
