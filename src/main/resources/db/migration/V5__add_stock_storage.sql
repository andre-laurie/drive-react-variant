-- Add new column STOCK so store current stock number. Cannot be null
ALTER TABLE PUBLIC.ARTICLE ADD COLUMN STOCK INTEGER DEFAULT 0 NOT NULL;

-- Fill STOCK with expression ranging between 0 and 9
UPDATE PUBLIC.ARTICLE SET STOCK=MOD(ID, 10);

-- Check STOCK in always greater or equals to 0
ALTER TABLE PUBLIC.ARTICLE ADD CONSTRAINT STOCK_POSITIVE CHECK (STOCK >= 0);