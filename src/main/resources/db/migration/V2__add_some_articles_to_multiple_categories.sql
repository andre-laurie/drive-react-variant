-- Adding some articles to category 'Boissons' to have some articles belonging to more than 1 category

INSERT INTO PUBLIC.ARTICLE_CATEGORY(ARTICLE_ID, CATEGORIES_ID) VALUES
(348924, 2),
(348925, 2),
(348923, 2),
(174283, 2),
(136874, 2),
(18221, 2),
(18220, 2);
