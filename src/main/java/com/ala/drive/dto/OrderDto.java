package com.ala.drive.dto;

import java.io.Serializable;
import java.util.Date;

import com.ala.drive.model.Order;
import com.ala.drive.model.Status;
import com.fasterxml.jackson.annotation.JsonFormat;

@SuppressWarnings("serial")
public class OrderDto implements Serializable {

    private int id;
    private int amount;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ssXXX")
    private Date createOn;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ssXXX")
    private Date deliveryDate;
    private Status currentStatus;

    public static OrderDto from(Order order) {
        OrderDto dto = new OrderDto();
        dto.id = order.getId();
        dto.amount = order.getAmount();
        dto.createOn = order.getCreatedOn();
        dto.deliveryDate = order.getDeliveryDate();
        dto.currentStatus = order.getCurrentStatus();
        return dto;
    }

    public int getId() {
        return id;
    }

    public int getAmount() {
        return amount;
    }

    public Date getCreateOn() {
        return createOn;
    }

    public Date getDeliveryDate() {
        return deliveryDate;
    }

    public Status getCurrentStatus() {
        return currentStatus;
    }

}
