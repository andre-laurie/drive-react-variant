package com.ala.drive.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import com.ala.drive.model.Article;

@SuppressWarnings("serial")
public class ArticleDto implements Serializable {
    private int id;
    private String name;
    private int qty;
    private int price;
    private String img;

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getQty() {
        return qty;
    }

    public int getPrice() {
        return price;
    }

    public String getImg() {
        return img;
    }

    public static ArticleDto from(Article article, int qty) {
        ArticleDto dto = new ArticleDto();
        dto.id = article.getId();
        dto.qty = qty;
        dto.name = article.getName();
        dto.img = article.getImg();
        dto.price = article.getPrice();
        return dto;
    }

    public static List<ArticleDto> frequencies(List<Article> articles) {
        Map<Integer, Article> articlesById = articles.stream()
                .collect(Collectors.toMap(Article::getId, Function.identity(), (a1, a2) -> a1));
        Map<Integer, Long> qtyById = articles.stream()
                .collect(Collectors.groupingBy(Article::getId, Collectors.counting()));

        return qtyById.entrySet()
                .stream()
                .map(e -> {
                    int articleId = e.getKey();
                    Long qty = e.getValue();
                    Article article = articlesById.get(articleId);
                    return ArticleDto.from(article, qty.intValue());

                })
                .collect(Collectors.toCollection(ArrayList::new));

    }
}
