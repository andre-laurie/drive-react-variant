package com.ala.drive.dto;

public class SimpleResponse {

    private Status status = Status.OK;
    private final String message;

    public SimpleResponse(Status status, String message) {
        super();
        this.status = status;
        this.message = message;
    }

    public enum Status {
        OK,
        ERROR
    }

    public Status getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

}
