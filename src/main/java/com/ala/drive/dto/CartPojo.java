package com.ala.drive.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.ala.drive.model.Article;
import com.ala.drive.model.Cart;

@SuppressWarnings("serial")
public class CartPojo implements Serializable {

    private Date createdOn;
    private List<Article> articles = new ArrayList<>();

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public Cart toCart() {
        var cart = new Cart();
        cart.setCreatedOn(this.getCreatedOn());
        cart.getArticles().addAll(getArticles());
        return cart;
    }

    public List<Article> getArticles() {
        return articles;
    }

    public void setArticles(List<Article> articles) {
        this.articles = articles;
    }

}
