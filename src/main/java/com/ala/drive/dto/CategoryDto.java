package com.ala.drive.dto;

import java.io.Serializable;

import com.ala.drive.model.Category;

@SuppressWarnings("serial")
public final class CategoryDto implements Serializable {

    private final int id;
    private final String name;

    private CategoryDto(int id, String name) {
        super();
        this.id = id;
        this.name = name;
    }

    public static CategoryDto from(Category category) {
        return new CategoryDto(category.getId(), category.getName());
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
