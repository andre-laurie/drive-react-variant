package com.ala.drive.dto;

import java.io.Serializable;
import java.util.List;

@SuppressWarnings("serial")
public class CartDto implements Serializable {

    private final List<ArticleDto> articles;
    private final int amount;

    public CartDto(List<ArticleDto> articles, int amount) {
        super();
        this.articles = articles;
        this.amount = amount;
    }

    public List<ArticleDto> getArticles() {
        return articles;
    }

    public int getAmount() {
        return amount;
    }

}
