package com.ala.drive.model;

public enum Status {

    ORDERED,
    READY_TO_DELIVER,
    DELIVERY_IN_PROGRESS,
    DELIVERED,
    CANCELED;

}
