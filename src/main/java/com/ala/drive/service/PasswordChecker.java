package com.ala.drive.service;

public interface PasswordChecker {

    String encode(String email, String password);

}
