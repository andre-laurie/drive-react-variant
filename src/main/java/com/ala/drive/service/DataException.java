package com.ala.drive.service;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class DataException extends Exception {
    private static final long serialVersionUID = 3634318263611299371L;

    public DataException(String msg) {
        super(msg);
    }

}
