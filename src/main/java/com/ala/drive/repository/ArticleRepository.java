package com.ala.drive.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.ala.drive.model.Article;
import com.ala.drive.model.Category;

public interface ArticleRepository extends CrudRepository<Article, Integer> {

    List<Article> findByCategoriesAndStockGreaterThanOrderByNameAsc(Category category, int stock);

    default List<Article> findByCategoriesAndAvailable(Category category) {
        return findByCategoriesAndStockGreaterThanOrderByNameAsc(category, 0);
    }

}
