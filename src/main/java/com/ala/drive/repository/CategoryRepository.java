package com.ala.drive.repository;

import java.util.List;

import com.ala.drive.model.Category;

import org.springframework.data.repository.CrudRepository;

public interface CategoryRepository extends CrudRepository<Category, Integer> {

    List<Category> findAllByOrderByOrderIdxAsc();

}
