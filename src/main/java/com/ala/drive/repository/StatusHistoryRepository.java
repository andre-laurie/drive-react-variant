package com.ala.drive.repository;

import com.ala.drive.model.StatusHistory;

import org.springframework.data.repository.CrudRepository;

public interface StatusHistoryRepository extends CrudRepository<StatusHistory, Integer> {

}
