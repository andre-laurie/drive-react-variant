package com.ala.drive.repository;

import java.util.List;
import java.util.Optional;

import com.ala.drive.model.Customer;
import com.ala.drive.model.Order;

import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.repository.CrudRepository;

public interface OrderRepository extends CrudRepository<Order, Integer> {

    List<Order> findByCustomerOrderByCreatedOnDesc(Customer customer);

    @Override
    @EntityGraph(attributePaths = "articles")
    Optional<Order> findById(Integer id);

}
