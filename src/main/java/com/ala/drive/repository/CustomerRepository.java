package com.ala.drive.repository;

import com.ala.drive.model.Customer;

import org.springframework.data.repository.CrudRepository;

public interface CustomerRepository extends CrudRepository<Customer, Integer> {

    Customer findCustomerByEmailAndPassword(String email, String encoded);

}
