package com.ala.drive.web;

import java.lang.invoke.MethodHandles;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.SessionAttribute;

import com.ala.drive.dto.ArticleDto;
import com.ala.drive.dto.CartPojo;
import com.ala.drive.dto.OrderDto;
import com.ala.drive.dto.SimpleResponse;
import com.ala.drive.dto.SimpleResponse.Status;
import com.ala.drive.model.Article;
import com.ala.drive.model.Customer;
import com.ala.drive.repository.CustomerRepository;
import com.ala.drive.repository.OrderRepository;
import com.ala.drive.service.LoginService;

@RestController
@RequestMapping("/customer")
public class CustomerController {

    private static final Logger LOGGER = LoggerFactory
            .getLogger(MethodHandles.lookup().lookupClass());

    private final CustomerRepository customerRepository;

    private final LoginService loginService;

    private final OrderRepository orderRepository;

    public CustomerController(CustomerRepository customerRepository,
            OrderRepository orderRepository, LoginService loginService) {
        this.customerRepository = customerRepository;
        this.orderRepository = orderRepository;
        this.loginService = loginService;
    }

    @PostMapping(value = "/login", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public SimpleResponse login(HttpSession session,
            @RequestParam String email,
            @RequestParam String password,
            @SessionAttribute CartPojo anonymousCart) {

        var customer = loginService.login(email, password, anonymousCart);
        if (customer == null) {
            return new SimpleResponse(Status.ERROR, "E-mail ou mot de passe invalide");
        } else {
            session.setAttribute("customerId", customer.getId());
            return new SimpleResponse(Status.OK, customer.getLogin());
        }
    }

    @PostMapping("/logout")
    public void logout(HttpSession session) {
        session.invalidate();
    }

    @GetMapping("/orders")
    public List<OrderDto> orders(
            @SessionAttribute Integer customerId) {

        var customer = customerRepository.findById(customerId).get();
        return orderRepository.findByCustomerOrderByCreatedOnDesc(customer).stream()
                .map(OrderDto::from)
                .collect(Collectors.toCollection(ArrayList::new));
    }

    @GetMapping("/order/{orderId}")
    public List<ArticleDto> order(
            @PathVariable int orderId) {

        var order = orderRepository.findById(orderId).orElseThrow();

        List<Article> articles = order.getArticles();
        return ArticleDto.frequencies(articles);
    }

    @GetMapping("/current")
    public SimpleResponse currentCustomer(@SessionAttribute(required = false) Integer customerId) {

        if (customerId == null) {
            return new SimpleResponse(Status.OK, "");
        }

        Optional<Customer> optCustomer = customerRepository.findById(customerId);
        if (optCustomer.isEmpty()) {
            return new SimpleResponse(Status.ERROR, "");
        } else {
            Customer customer = optCustomer.get();
            return new SimpleResponse(Status.OK, customer.getLogin());
        }
    }

}
