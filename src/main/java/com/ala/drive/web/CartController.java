package com.ala.drive.web;

import java.lang.invoke.MethodHandles;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.SessionAttribute;

import com.ala.drive.dto.ArticleDto;
import com.ala.drive.dto.CartDto;
import com.ala.drive.dto.CartPojo;
import com.ala.drive.dto.SimpleResponse;
import com.ala.drive.dto.SimpleResponse.Status;
import com.ala.drive.model.Article;
import com.ala.drive.repository.ArticleRepository;
import com.ala.drive.repository.CustomerRepository;
import com.ala.drive.service.LoginService;

@RestController
@RequestMapping("/cart")
public class CartController {

    private static final String WORKING_WITH_TRANSIENT_CART = "Working with transient cart";

    private static final Logger LOGGER = LoggerFactory
            .getLogger(MethodHandles.lookup().lookupClass());

    private final ArticleRepository articleRepository;

    private final CustomerRepository customerRepository;

    private final LoginService loginService;

    public CartController(ArticleRepository articleRepository,
            CustomerRepository customerRepository,
            LoginService loginService) {
        this.articleRepository = articleRepository;
        this.customerRepository = customerRepository;
        this.loginService = loginService;
    }

    @GetMapping(path = "/preview")
    public CartDto preview(@SessionAttribute(required = false) Integer customerId,
            @SessionAttribute(required = false) CartPojo anonymousCart) {

        List<Article> articles;
        if (customerId == null) {
            LOGGER.info(WORKING_WITH_TRANSIENT_CART);
            articles = anonymousCart.getArticles();
        } else {
            var customer = customerRepository.findById(customerId).orElseThrow();
            var activeCart = customer.getActiveCart();
            articles = activeCart.getArticles();
            LOGGER.info("Working with persisted cart {}", activeCart.getId());
        }

        List<ArticleDto> articlesDto = ArticleDto.frequencies(articles);

        int amount = articles.stream()
                .mapToInt(Article::getPrice)
                .sum();

        return new CartDto(articlesDto, amount);
    }

    @PostMapping(path = "/add", consumes = "application/json", produces = "application/json")
    public SimpleResponse add(@RequestBody int articleId,
            @SessionAttribute(required = false) Integer customerId,
            @SessionAttribute(required = false) CartPojo anonymousCart) {

        var optArticle = articleRepository.findById(articleId);
        if (optArticle.isEmpty()) {
            return new SimpleResponse(Status.ERROR, "Unexpected article: " + articleId);
        }

        Article article = optArticle.get();

        if (customerId == null) {
            LOGGER.info(WORKING_WITH_TRANSIENT_CART);
            LOGGER.info("Adding article: article.id={} to transient cart", articleId);
            anonymousCart.getArticles().add(article);
        } else {
            LOGGER.info("Adding article: article.id={} to persisted cart", articleId);
            loginService.addToCart(customerId, article);
        }

        return new SimpleResponse(Status.OK, "");
    }

    @PostMapping(path = "/remove", consumes = "application/json", produces = "application/json")
    public SimpleResponse remove(@RequestBody int articleId,
            @SessionAttribute(required = false) Integer customerId,
            @SessionAttribute(required = false) CartPojo anonymousCart) {

        var optArticle = articleRepository.findById(articleId);
        if (optArticle.isEmpty()) {
            return new SimpleResponse(Status.ERROR, "Unexpected article: " + articleId);
        }

        var article = optArticle.get();

        if (customerId == null) {
            LOGGER.info(WORKING_WITH_TRANSIENT_CART);
            LOGGER.info("Removing article: article.id={} to transient cart", articleId);
            Optional<Article> findAny = anonymousCart.getArticles().stream()
                    .filter(a -> a.getId() == article.getId())
                    .findAny();
            findAny.ifPresent(a -> anonymousCart.getArticles()
                    .remove(a));
        } else {
            LOGGER.info("Removing article: article.id={} to persisted cart", articleId);
            loginService.removeFromCart(customerId, article);
        }

        return new SimpleResponse(Status.OK, "");
    }

    @PostMapping("/validate")
    public SimpleResponse validate(
            @SessionAttribute Integer customerId) {

        List<Article> missingArticles = loginService.validateOrder(customerId);

        if (missingArticles.isEmpty()) {
            LOGGER.info("No missing articles, order validated, proceed with order display");
            return new SimpleResponse(Status.OK,
                    "Votre commande a bien été validée");
        }

        LOGGER.info("Missing articles while validating, cart is fixed, back to review");
        var missingArticlesAsString = missingArticles.stream()
                .map(Article::getName)
                .distinct()
                .sorted()
                .collect(Collectors.joining(", "));
        var error = String.format(
                """
                        Les articles suivants étaient indisponibles dans la quantité souhaitée.
                        Ils ont été retirés du panier : %s""",
                missingArticlesAsString);

        return new SimpleResponse(Status.ERROR, error);
    }

}
