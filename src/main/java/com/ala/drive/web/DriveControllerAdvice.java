package com.ala.drive.web;

import java.lang.invoke.MethodHandles;
import java.util.Date;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.SessionAttribute;

import com.ala.drive.dto.CartPojo;

@ControllerAdvice
public class DriveControllerAdvice {

    private static final Logger LOGGER = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    @ModelAttribute
    public void addTransientCartIntoSession(HttpSession session,
            @SessionAttribute(required = false) Integer customerId,
            @SessionAttribute(required = false) CartPojo anonymousCart) {

        LOGGER.info("customerId={} anonymousCart={}", customerId, anonymousCart);
        if (customerId == null && anonymousCart == null) {
            // Setup transient cart into session for anonymous visitors
            LOGGER.info("Anonymous visitor and no cart in session: adding new Cart to session");
            var newCart = new CartPojo();
            newCart.setCreatedOn(new Date());
            session.setAttribute("anonymousCart", newCart);
        }
    }

}
