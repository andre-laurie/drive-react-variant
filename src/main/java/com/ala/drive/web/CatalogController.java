package com.ala.drive.web;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ala.drive.dto.ArticleDto;
import com.ala.drive.dto.CategoryDto;
import com.ala.drive.model.Article;
import com.ala.drive.repository.ArticleRepository;
import com.ala.drive.repository.CategoryRepository;
import com.ala.drive.service.DataException;

@RestController
@RequestMapping("/catalog")
public class CatalogController {

    private final CategoryRepository categoryRepository;
    private final ArticleRepository articleRepository;

    public CatalogController(CategoryRepository categoryRepository,
            ArticleRepository articleRepository) {
        this.categoryRepository = categoryRepository;
        this.articleRepository = articleRepository;
    }

    @GetMapping(path = { "/categories" })
    public List<CategoryDto> categories() {

        return categoryRepository.findAllByOrderByOrderIdxAsc().stream()
                .map(CategoryDto::from)
                .collect(Collectors.toCollection(ArrayList::new));
    }

    @GetMapping(path = "/category/{categoryId}")
    public List<ArticleDto> category(@PathVariable int categoryId)
            throws DataException {

        var optCategory = categoryRepository.findById(categoryId);
        if (optCategory.isEmpty()) {
            throw new DataException("Category not found: " + categoryId);
        }

        var category = optCategory.get();

        List<Article> articles = articleRepository.findByCategoriesAndAvailable(category);

        return articles.stream()
                .map(article -> {
                    int nbInStock = article.getStock();
                    return ArticleDto.from(article, nbInStock);
                }).collect(Collectors.toCollection(ArrayList::new));
    }

}
