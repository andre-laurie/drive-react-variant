package com.ala.drive;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.ala.drive.model.Category;
import com.ala.drive.repository.CategoryRepository;

@SpringBootApplication
public class DriveApplication {

    public static void main(String[] args) {
        SpringApplication.run(DriveApplication.class, args);
    }

    @Bean
    public CommandLineRunner demo(CategoryRepository categoryRepository) {
        return (args) -> {
            Iterable<Category> listCategory = categoryRepository.findAll();

            /*
             * Testing:
             * - use Flyway plugin to create and populate db
             * - start JPA in validate mode
             * - access db
             */
            for (Category c : listCategory) {
                System.out.println("XXX Category: %s".formatted(c.getName()));
            }
        };
    }

}
