package com.ala.drive.service;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;

public class MD5CheckerTest {

    private final MD5Checker md5Checker = new MD5Checker();

    @Test
    public void testEncodedValues() {
        assertThat(md5Checker.encode("a@a", "a")).isEqualTo("BGfeiTOXva/mSnoluSgIYA==");
        assertThat(md5Checker.encode("b@b", "b")).isEqualTo("/znlqlTcvx3A5zsG+M38wA==");
        assertThat(md5Checker.encode("c@c", "c")).isEqualTo("XVka9B+wG/3wAIIkED90BQ==");
    }

}
