package com.ala.drive;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(properties = { "spring.jpa.hibernate.ddl-auto=create" })
@AutoConfigureTestDatabase
class DriveApplicationTests {

    @Test
    void contextLoads() {
    }

}
