package com.ala.drive.web;

import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.BDDMockito;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import com.ala.drive.model.Article;
import com.ala.drive.model.Category;
import com.ala.drive.repository.ArticleRepository;
import com.ala.drive.repository.CategoryRepository;
import com.ala.drive.service.DataException;

@ExtendWith(MockitoExtension.class)
public class CatalogControllerTest {

    @Mock
    private CategoryRepository categoryRepository;
    @Mock
    private ArticleRepository articleRepository;

    private MockMvc mockMvc;

    private CatalogController catalogController;

    @BeforeEach
    public void before() {
        catalogController = new CatalogController(categoryRepository, articleRepository);
        mockMvc = standaloneSetup(catalogController)
                .setControllerAdvice(new DriveControllerAdvice())
                .build();
    }

    @Test
    public void whenCategories_thenOk() throws Exception {
        Category c = new Category();
        c.setName("category");
        List<Category> categories = List.of(c);

        BDDMockito.given(categoryRepository.findAllByOrderByOrderIdxAsc())
                .willReturn(categories);

        mockMvc.perform(get("/catalog/categories"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].name", is("category")));

    }

    @Test
    public void givenUnknownCategory_whenCategory_thenException() throws Exception {
        BDDMockito.given(categoryRepository.findById(ArgumentMatchers.anyInt()))
                .willReturn(Optional.empty());

        mockMvc.perform(get("/catalog/category/{categoryId}", 0))
                .andExpect(status().isBadRequest())
                .andExpect(result -> assertTrue(
                        result.getResolvedException() instanceof DataException));

    }

    @Test
    public void givenValidCategory_whenCategory_thenArticles() throws Exception {
        Category category = new Category();
        BDDMockito.given(categoryRepository.findById(ArgumentMatchers.anyInt()))
                .willReturn(Optional.of(category));

        Article article = new Article();
        article.setName("article");
        List<Article> articles = List.of(article);

        BDDMockito.given(articleRepository.findByCategoriesAndAvailable(category))
                .willReturn(articles);

        mockMvc.perform(get("/catalog/category/{categoryId}", 0))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].name", is("article")));
    }

}
