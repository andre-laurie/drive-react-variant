package com.ala.drive.web;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.tuple;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import com.ala.drive.model.Article;
import com.ala.drive.model.Cart;
import com.ala.drive.model.Category;
import com.ala.drive.model.Customer;
import com.ala.drive.model.Order;
import com.ala.drive.model.Perishable;
import com.ala.drive.model.Product;
import com.ala.drive.model.Status;
import com.ala.drive.model.StatusHistory;
import com.ala.drive.repository.ArticleRepository;
import com.ala.drive.repository.CategoryRepository;
import com.ala.drive.repository.CustomerRepository;
import com.ala.drive.repository.OrderRepository;

@DataJpaTest(properties = { "spring.jpa.hibernate.ddl-auto=create" })
public class ModelIntegrationTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private ArticleRepository articleRepository;

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private OrderRepository orderRepository;

    @Test
    public void whenMappingArticle_thenOk() {
        Article article = new Article();
        article.setName("article");
        article.setImg("img");
        article.setPrice(1000);
        article.setStock(1);
        article.setVat(2.);
        article.setEan("ean");
        article = entityManager.persistAndFlush(article);

        assertThat(article).extracting(
                Article::getName,
                Article::getImg,
                Article::getPrice,
                Article::getStock,
                Article::getVat,
                Article::getEan)
                .containsExactly("article", "img", 1000, 1, 2., "ean");
    }

    @Test
    public void whenMappingProduct_thenOk() {
        Product product = new Product();
        product.setName("article");
        product.setImg("img");
        product.setPrice(1000);
        product.setStock(1);
        product.setVat(2.);
        product.setEan("ean");
        product = entityManager.persistAndFlush(product);

        assertThat(product.getName()).isEqualTo("article");
    }

    @Test
    public void whenMappingPerishable_thenOk() {
        Perishable perishable = new Perishable();
        perishable.setName("article");
        perishable.setImg("img");
        perishable.setPrice(1000);
        perishable.setStock(1);
        perishable.setVat(2.);
        perishable.setEan("ean");
        perishable.setLot("lot");

        LocalDate localDate = LocalDate.of(2000, 1, 1);
        Instant instant = localDate.atStartOfDay(ZoneOffset.systemDefault()).toInstant();
        Date date = Date.from(instant);
        perishable.setBestBefore(date);
        perishable = entityManager.persistAndFlush(perishable);

        assertThat(perishable).extracting(Perishable::getBestBefore, Perishable::getLot)
                .containsExactly(date, "lot");
    }

    @Test
    public void whenMappingCategory_thenOk() {
        Category category = new Category();
        category.setName("category");
        category.setOrderIdx(1);
        category = entityManager.persistAndFlush(category);

        assertThat(category.getName()).isEqualTo("category");
        assertThat(category.getOrderIdx()).isEqualTo(1);
    }

    @Test
    public void givenUnavailableArticles_whenFindByCategoriesAndAvailable_thenOnlyReturnAvailableArticles()
            throws Exception {
        Category cat = new Category();
        cat.setName("cat");
        cat.setOrderIdx(0);
        cat = entityManager.persist(cat);

        Article articleAvailable = new Article();
        articleAvailable.setName("available");
        articleAvailable.setStock(1);
        articleAvailable.getCategories().add(cat);
        articleAvailable = entityManager.persist(articleAvailable);

        Article articleUnavailable = new Article();
        articleUnavailable.setName("unavailable");
        articleUnavailable.setStock(0);
        articleUnavailable.getCategories().add(cat);
        articleUnavailable = entityManager.persistAndFlush(articleUnavailable);

        List<Article> articles = articleRepository.findByCategoriesAndAvailable(cat);
        assertThat(articles).containsExactly(articleAvailable);
    }

    @Test
    public void givenTwoCategories_whenFindAllByOrderByOrderIdxAsc_thenCategoriesCorrectlySorted() {
        Category cat1 = new Category();
        cat1.setName("cat1");
        cat1.setOrderIdx(2);
        entityManager.persist(cat1);

        Category cat2 = new Category();
        cat2.setName("cat2");
        cat2.setOrderIdx(1);
        entityManager.persistAndFlush(cat2);

        List<Category> categories = categoryRepository.findAllByOrderByOrderIdxAsc();
        assertThat(categories).extracting(Category::getName)
                .containsExactly("cat2", "cat1");
    }

    @Test
    public void givenArticleInTwoCategories_whenFindByCategories_thenFoundInBoth()
            throws Exception {
        Category cat1 = new Category();
        cat1.setName("cat1");
        cat1.setOrderIdx(1);
        entityManager.persist(cat1);

        Category cat2 = new Category();
        cat2.setName("cat2");
        cat2.setOrderIdx(2);
        entityManager.persist(cat2);

        Article article = new Article();
        article.setName("article");
        article.setStock(1);
        article.getCategories().addAll(List.of(cat1, cat2));
        article = entityManager.persistAndFlush(article);

        List<Article> articlesInCat1 = articleRepository.findByCategoriesAndAvailable(cat1);
        assertThat(articlesInCat1).containsExactly(article);
        List<Article> articlesInCat2 = articleRepository.findByCategoriesAndAvailable(cat2);
        assertThat(articlesInCat2).containsExactly(article);
    }

    @Test
    public void whenArticleInCart_thenMappingOk() {
        Article article = new Article();
        article = entityManager.persist(article);

        Cart cart = new Cart();
        cart.getArticles().addAll(List.of(article, article));
        cart = entityManager.persistAndFlush(cart);

        assertThat(cart.getArticles()).containsExactly(article, article);
    }

    @Test
    public void whenCustomerCart_thenMappingOk() {
        Customer customer = new Customer();
        customer.setLogin("login");
        customer.setEmail("email");
        customer.setPassword("password");
        customer.setFirstName("firstname");
        customer.setLastName("lastname");
        customer = entityManager.persist(customer);

        Cart cart1 = new Cart();
        cart1.setCustomer(customer);
        cart1 = entityManager.persist(cart1);

        Cart cart2 = new Cart();
        cart2.setCustomer(customer);
        cart2 = entityManager.persist(cart2);

        customer.setActiveCart(cart2);
        customer = entityManager.merge(customer);
        entityManager.flush();

        Customer foundCustomer = customerRepository.findCustomerByEmailAndPassword("email",
                "password");
        assertThat(foundCustomer).isEqualTo(customer)
                .extracting(Customer::getLogin, Customer::getEmail, Customer::getPassword,
                        Customer::getFirstName, Customer::getLastName, Customer::getActiveCart)
                .containsExactly("login", "email", "password", "firstname", "lastname", cart2);
    }

    @Test
    public void whenCustomerOrder_thenMapppingOk() {
        Customer customer = new Customer();
        customer = entityManager.persist(customer);

        Article article = new Article();
        article = entityManager.persist(article);

        Order order1 = new Order();
        order1.setAmount(1000);
        LocalDate localDate = LocalDate.of(2000, 1, 1);
        Instant instantCreation = localDate.atStartOfDay(ZoneOffset.systemDefault()).toInstant();
        Date dateCreation = Date.from(instantCreation);
        order1.setCreatedOn(dateCreation);
        Instant instantDelivery = instantCreation.plus(3, ChronoUnit.DAYS);
        Date dateDelivery = Date.from(instantDelivery);
        order1.setDeliveryDate(dateDelivery);
        order1.setCustomer(customer);
        order1.getArticles().addAll(List.of(article, article));
        order1.setCurrentStatus(Status.ORDERED);
        order1 = entityManager.persistAndFlush(order1);
        Order order2 = new Order();
        order2.setAmount(1000);
        LocalDate localDate2 = LocalDate.of(2001, 1, 1);
        Instant instantCreation2 = localDate2.atStartOfDay(ZoneOffset.systemDefault()).toInstant();
        Date dateCreation2 = Date.from(instantCreation2);
        order2.setCreatedOn(dateCreation2);
        Instant instantDelivery2 = instantCreation2.plus(3, ChronoUnit.DAYS);
        Date dateDelivery2 = Date.from(instantDelivery2);
        order2.setDeliveryDate(dateDelivery2);
        order2.setCustomer(customer);
        order2.getArticles().addAll(List.of(article, article));
        order2.setCurrentStatus(Status.ORDERED);
        order2 = entityManager.persistAndFlush(order2);

        List<Order> orders = orderRepository.findByCustomerOrderByCreatedOnDesc(customer);
        assertThat(orders).containsExactly(order2, order1)
                .extracting(Order::getAmount, Order::getCreatedOn, Order::getDeliveryDate,
                        Order::getCustomer, Order::getCurrentStatus)
                .containsExactly(
                        tuple(1000, dateCreation2, dateDelivery2, customer, Status.ORDERED),
                        tuple(1000, dateCreation, dateDelivery, customer, Status.ORDERED));
    }

    @Test
    public void whenOrderHistory_thenMappingOk() {
        StatusHistory statusHistory = new StatusHistory();
        statusHistory.setStatus(Status.ORDERED);
        LocalDate localDate = LocalDate.of(2001, 1, 1);
        Instant instant = localDate.atStartOfDay(ZoneOffset.systemDefault()).toInstant();
        Date date = Date.from(instant);
        statusHistory.setStatusDate(date);
        entityManager.persist(statusHistory);

        Order order = new Order();
        order.getHistory().add(statusHistory);
        order = entityManager.persistAndFlush(order);

        assertThat(order.getHistory()).hasSize(1)
                .element(0)
                .extracting(StatusHistory::getStatus, StatusHistory::getStatusDate)
                .containsExactly(Status.ORDERED, date);
    }

}
