package com.ala.drive.web;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.BDDMockito;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.web.servlet.MockMvc;

import com.ala.drive.dto.CartPojo;
import com.ala.drive.dto.SimpleResponse.Status;
import com.ala.drive.model.Article;
import com.ala.drive.model.Customer;
import com.ala.drive.model.Order;
import com.ala.drive.repository.CustomerRepository;
import com.ala.drive.repository.OrderRepository;
import com.ala.drive.service.LoginService;

@ExtendWith(MockitoExtension.class)
public class CustomerControllerTest {

    @Mock
    private CustomerRepository customerRepository;

    @Mock
    private OrderRepository orderRepository;

    @Mock
    private LoginService loginService;

    private CustomerController customerController;
    private MockMvc mockMvc;

    @BeforeEach
    public void before() {
        customerController = new CustomerController(customerRepository, orderRepository,
                loginService);
        mockMvc = standaloneSetup(customerController)
                .setControllerAdvice(new DriveControllerAdvice())
                .build();
    }

    @Test
    public void givenUnknownCredentials_whenLogin_thenError()
            throws Exception {
        BDDMockito
                .given(loginService.login(ArgumentMatchers.anyString(),
                        ArgumentMatchers.anyString(), ArgumentMatchers.any(CartPojo.class)))
                .willReturn(null);

        mockMvc.perform(post("/customer/login")
                .contentType(MediaType.MULTIPART_FORM_DATA_VALUE)
                .param("email", "email")
                .param("password", "password"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.status").value(Status.ERROR.name()))
                .andExpect(jsonPath("$.message").value("E-mail ou mot de passe invalide"));
    }

    @Test
    public void givenValidCredentials_whenLogin_thenOk() throws Exception {
        Customer customer = new Customer();
        customer.setId(1);
        customer.setLogin("login");
        BDDMockito
                .given(loginService.login(ArgumentMatchers.anyString(),
                        ArgumentMatchers.anyString(), ArgumentMatchers.any(CartPojo.class)))
                .willReturn(customer);

        MockHttpSession session = new MockHttpSession();

        mockMvc.perform(post("/customer/login")
                .session(session)
                .contentType(MediaType.MULTIPART_FORM_DATA_VALUE)
                .param("email", "email")
                .param("password", "password"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.status").value(Status.OK.name()))
                .andExpect(jsonPath("$.message").value("login"));

        assertThat(session.getAttribute("customerId")).isEqualTo(1);
    }

    @Test
    public void whenLogout_thenSessionInvalidated() throws Exception {
        MockHttpSession session = new MockHttpSession();

        mockMvc.perform(post("/customer/logout")
                .session(session))
                .andExpect(status().isOk());

        assertTrue(session.isInvalid());
    }

    @Test
    public void whenOrders_thenOrderList() throws Exception {
        BDDMockito.given(customerRepository.findById(1)).willReturn(Optional.of(new Customer()));

        Order order = new Order();
        order.setAmount(10);
        List<Order> orders = List.of(order);
        BDDMockito.given(orderRepository.findByCustomerOrderByCreatedOnDesc(ArgumentMatchers.any()))
                .willReturn(orders);

        mockMvc.perform(get("/customer/orders")
                .sessionAttr("customerId", 1))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.size()").value(1))
                .andExpect(jsonPath("$[0].amount").value(10));
    }

    @Test
    public void whenOrder_thenOrderContents() throws Exception {
        Order order = new Order();
        Article article = new Article();
        article.setName("name");
        order.getArticles().add(article);

        BDDMockito.given(orderRepository.findById(2)).willReturn(Optional.of(order));

        mockMvc.perform(get("/customer/order/{orderId}", 2)
                .sessionAttr("customerId", 1))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.size()").value(1))
                .andExpect(jsonPath("$[0].name").value("name"));
    }

    @Test
    public void givenTwoSameArticles_whenOrder_thenQtyIs2() throws Exception {
        Order order = new Order();
        Article article = new Article();
        article.setName("name");
        order.getArticles().addAll(List.of(article, article));

        BDDMockito.given(orderRepository.findById(2)).willReturn(Optional.of(order));

        mockMvc.perform(get("/customer/order/{orderId}", 2)
                .sessionAttr("customerId", 1))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.size()").value(1))
                .andExpect(jsonPath("$[0].name").value("name"))
                .andExpect(jsonPath("$[0].qty").value(2));
    }

    @Test
    public void givenAnonymous_whenCurrentCustomer_thenOk() throws Exception {
        mockMvc.perform(get("/customer/current"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.status").value(Status.OK.name()))
                .andExpect(jsonPath("$.message").value(""));
    }

    @Test
    public void givenInvalidCustomerId_whenCurrentCustomer_thenError() throws Exception {
        BDDMockito.given(customerRepository.findById(1)).willReturn(Optional.empty());

        mockMvc.perform(get("/customer/current")
                .sessionAttr("customerId", 1))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.status").value(Status.ERROR.name()))
                .andExpect(jsonPath("$.message").value(""));
    }

    @Test
    public void givenValidCustomerId_whenCurrentCustomer_thenOk() throws Exception {
        Customer customer = new Customer();
        customer.setLogin("login");
        BDDMockito.given(customerRepository.findById(1)).willReturn(Optional.of(customer));

        mockMvc.perform(get("/customer/current")
                .sessionAttr("customerId", 1))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.status").value(Status.OK.name()))
                .andExpect(jsonPath("$.message").value("login"));
    }

}
