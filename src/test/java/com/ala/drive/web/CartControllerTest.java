package com.ala.drive.web;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.BDDMockito;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import com.ala.drive.dto.CartPojo;
import com.ala.drive.dto.SimpleResponse.Status;
import com.ala.drive.model.Article;
import com.ala.drive.model.Cart;
import com.ala.drive.model.Customer;
import com.ala.drive.repository.ArticleRepository;
import com.ala.drive.repository.CustomerRepository;
import com.ala.drive.service.LoginService;
import com.fasterxml.jackson.databind.ObjectMapper;

@ExtendWith(MockitoExtension.class)
public class CartControllerTest {

    @Mock
    private ArticleRepository articleRepository;

    @Mock
    private CustomerRepository customerRepository;

    @Mock
    private LoginService loginService;

    private CartController cartController;

    private MockMvc mockMvc;

    @BeforeEach
    public void before() {
        cartController = new CartController(articleRepository, customerRepository, loginService);
        mockMvc = standaloneSetup(cartController)
                .setControllerAdvice(new DriveControllerAdvice())
                .build();
    }

    @Test
    public void givenAnonymous_whenPreview_thenContentsFromSessionCart() throws Exception {
        CartPojo anonymousCart = new CartPojo();
        Article article = new Article();
        article.setId(144818);
        article.setName("article");
        article.setImg("img");
        article.setPrice(1234);
        article.setStock(10);
        anonymousCart.setArticles(List.of(article));

        mockMvc.perform(get("/cart/preview")
                .sessionAttr("anonymousCart", anonymousCart))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.amount", is(1234)))
                .andExpect(jsonPath("$.articles[0].id", is(144818)))
                .andExpect(jsonPath("$.articles[0].name", is("article")))
                .andExpect(jsonPath("$.articles[0].img", is("img")))
                .andExpect(jsonPath("$.articles[0].price", is(1234)))
                .andExpect(jsonPath("$.articles[0].qty", is(1)));
    }

    @Test
    public void givenTwoSameArticles_whenPreview_thenQtyIs2AndAmountOk() throws Exception {
        CartPojo anonymousCart = new CartPojo();
        Article article = new Article();
        article.setId(144818);
        article.setName("article");
        article.setImg("img");
        article.setPrice(1000);
        article.setStock(10);
        anonymousCart.setArticles(List.of(article, article));

        mockMvc.perform(get("/cart/preview")
                .sessionAttr("anonymousCart", anonymousCart))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.amount", is(2000)))
                .andExpect(jsonPath("$.articles.size()", is(1)))
                .andExpect(jsonPath("$.articles[0].id", is(144818)))
                .andExpect(jsonPath("$.articles[0].price", is(1000)))
                .andExpect(jsonPath("$.articles[0].qty", is(2)));
    }

    @Test
    public void givenAuthenticated_whenPreview_thenContentsFromDB() throws Exception {
        Cart persistedCart = new Cart();
        Article article = new Article();
        article.setId(144818);
        article.setName("article");
        article.setImg("img");
        article.setPrice(1234);
        article.setStock(10);

        persistedCart.getArticles().add(article);

        Customer customer = new Customer();
        customer.setActiveCart(persistedCart);
        BDDMockito.given(customerRepository.findById(1)).willReturn(Optional.of(customer));

        mockMvc.perform(get("/cart/preview")
                .sessionAttr("customerId", 1))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.amount", is(1234)))
                .andExpect(jsonPath("$.articles[0].id", is(144818)))
                .andExpect(jsonPath("$.articles[0].name", is("article")))
                .andExpect(jsonPath("$.articles[0].img", is("img")))
                .andExpect(jsonPath("$.articles[0].price", is(1234)))
                .andExpect(jsonPath("$.articles[0].qty", is(1)));
    }

    @Test
    public void givenInvalidArticle_whenAdd_thenResponseError() throws Exception {
        CartPojo anonymousCart = new CartPojo();

        mockMvc.perform(post("/cart/add")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(1000))
                .sessionAttr("anonymousCart", anonymousCart))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.status").value(Status.ERROR.name()))
                .andExpect(jsonPath("$.message").value("Unexpected article: 1000"));

        assertThat(anonymousCart.getArticles()).isEmpty();
    }

    @Test
    public void givenAnonymousAndValidInput_whenAdd_thenArticleAdded() throws Exception {
        CartPojo anonymousCart = new CartPojo();

        Article article = new Article();
        BDDMockito.given(articleRepository.findById(1)).willReturn(Optional.of(article));

        mockMvc.perform(post("/cart/add")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(1))
                .sessionAttr("anonymousCart", anonymousCart))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.status").value(Status.OK.name()));

        assertThat(anonymousCart.getArticles()).containsExactly(article);
    }

    @Test
    public void givenAuthenticatedAndValidInput_whenAdd_thenArticleAdded() throws Exception {
        Article article = new Article();
        BDDMockito.given(articleRepository.findById(1)).willReturn(Optional.of(article));

        mockMvc.perform(post("/cart/add")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(1))
                .sessionAttr("customerId", 1))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.status").value(Status.OK.name()));

        BDDMockito.then(loginService).should().addToCart(1, article);
    }

    @Test
    public void givenInvalidArticle_whenRemove_thenResponseError() throws Exception {
        var anonymousCart = new CartPojo();

        mockMvc.perform(post("/cart/remove")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(1000))
                .sessionAttr("anonymousCart", anonymousCart))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.status").value(Status.ERROR.name()))
                .andExpect(jsonPath("$.message").value("Unexpected article: 1000"));

        assertThat(anonymousCart.getArticles()).isEmpty();
    }

    @Test
    public void givenAnonymousAndValidInput_whenRemove_thenArticleRemoved() throws Exception {
        Article article = new Article();
        article.setId(1);
        List<Article> articles = new ArrayList<>();
        var anonymousCart = new CartPojo();
        articles.add(article);
        articles.add(article);
        anonymousCart.setArticles(articles);

        BDDMockito.given(articleRepository.findById(1)).willReturn(Optional.of(article));

        mockMvc.perform(post("/cart/remove")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(1))
                .sessionAttr("anonymousCart", anonymousCart))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.status").value(Status.OK.name()));

        assertThat(anonymousCart.getArticles()).containsExactly(article);
    }

    @Test
    public void givenAnonymousAndValidInputAndArticleNotInCart_whenRemove_thenDoNothing()
            throws Exception {
        Article article = new Article();
        article.setId(2);
        List<Article> articles = new ArrayList<>();
        articles.add(article);
        var anonymousCart = new CartPojo();
        anonymousCart.setArticles(articles);

        Article articleNotInCart = new Article();
        articleNotInCart.setId(1);
        BDDMockito.given(articleRepository.findById(1)).willReturn(Optional.of(articleNotInCart));

        mockMvc.perform(post("/cart/remove")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(1))
                .sessionAttr("anonymousCart", anonymousCart))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.status").value(Status.OK.name()));

        assertThat(anonymousCart.getArticles()).containsExactly(article);
    }

    @Test
    public void givenAuthenticatedAndValidInput_whenRemove_thenArticleRemoved() throws Exception {
        Article article = new Article();
        BDDMockito.given(articleRepository.findById(1)).willReturn(Optional.of(article));

        mockMvc.perform(post("/cart/remove")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(1))
                .sessionAttr("customerId", 1))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.status").value(Status.OK.name()));

        BDDMockito.then(loginService).should().removeFromCart(1, article);
    }

    @Test
    public void givenNoMissingArticles_whenValidate_thenOrder() throws Exception {
        BDDMockito.given(loginService.validateOrder(1)).willReturn(List.of());

        mockMvc.perform(post("/cart/validate")
                .sessionAttr("customerId", 1))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.status").value(Status.OK.name()));
    }

    @Test
    public void givenMissingArticles_whenValidate_thenReviewFixedCart() throws Exception {
        Article article = new Article();
        article.setName("Article1");
        BDDMockito.given(loginService.validateOrder(1)).willReturn(List.of(article));

        var error = String.format(
                """
                        Les articles suivants étaient indisponibles dans la quantité souhaitée.
                        Ils ont été retirés du panier : %s""",
                "Article1");

        mockMvc.perform(post("/cart/validate")
                .sessionAttr("customerId", 1))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.status").value(Status.ERROR.name()))
                .andExpect(jsonPath("$.message").value(error));
    }

}
